#ifndef ENTITY_H
#define ENTITY_H

#include <raylib.h>
#include <vector>

class Entity {
public:
  virtual void Move(float deltaTime) = 0;
  virtual void Draw() = 0;
};

extern std::vector<Entity *> entities;

#endif // ENTITY_H
