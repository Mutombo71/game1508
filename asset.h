#ifndef ASSET_H
#define ASSET_H

#include <raylib.h>
#include <string>
#include <unordered_map>

std::unordered_map<std::string, Rectangle> assets {
  { "Stone0", {48, 0, 16, 16} },
  { "Stone1", {64, 0, 16, 16} },
  { "Stone2", {48, 16, 16, 16} },
  { "Stone3", {64, 16, 16, 16} },
  { "Stone4", {80, 0, 32, 32} },
  { "Stone5", {80, 32, 32, 48} },
  { "Seedling0", {0, 0, 16, 16} },
  { "Seedling1", {16, 0, 16, 16} },
  { "Seedling2", {32, 0, 16, 16} },
  { "Bush0", {0, 96, 64, 48} },
  { "Bush1", {0, 144, 64, 48} },
  { "Plant0", {0, 16, 16, 16} },
  { "Plant1", {32, 32, 16, 16} },
  { "Plant2", {0, 32, 32, 32} },
  { "Chest0", {112, 0, 32, 32} },
  { "Chest1", {112, 32, 32, 32} },
  { "Chest2", {112, 64, 32, 16} },
};

#endif // ASSET_H
