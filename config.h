#ifndef CONFIG
#define CONFIG

const int tilesize = 32;
const int mapWidth = 48;
const int mapHeight = 27;
const int screenWidth = mapWidth * tilesize;
const int screenHeight = mapHeight * tilesize;

#endif
