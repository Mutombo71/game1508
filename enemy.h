#include "entity.h"
#include <raylib.h>

class Enemy : public Entity {
public:
  Enemy(Vector2 position);
  void Move(float deltaTime) override;
  void Draw() override;

private:
  Vector2 position;
  Vector2 velocity;
  float maxSpeed;
};

Enemy::Enemy(Vector2 position)
    : position(position), velocity({100.0f, 0.0f}), maxSpeed(100.0f) {}

void Enemy::Move(float deltaTime) {
  position.x += velocity.x * deltaTime;

  if (position.x - 15.0f <= 0 || position.x >= GetScreenWidth() - 15.0f) {
    velocity.x = -velocity.x;
  }
}

void Enemy::Draw() { DrawCircle(position.x, position.y, 15, BLUE); }
