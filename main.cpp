#include "config.h"
#include "enemy.h"
#include "entity.h"
#include "map.h"
#include "player.h"
#include <raylib.h>
#include <vector>

std::vector<Entity *> entities;

int main(void) {

  InitWindow(screenWidth, screenHeight, "Game 1508");

  Map map = Map();

  Player player({(float)screenWidth / 2.0f, (float)screenHeight / 2.0f});
  entities.push_back(&player);

  Camera2D camera;
  camera.target = {player.GetPosition()};
  camera.offset = {(float)screenWidth / 2.0, screenHeight / 2.0};
  camera.rotation = 0.0f;
  camera.zoom = 2.0f;

  // Enemy enemy({(float)screenWidth / 3.0f, (float)screenHeight / 3.0f});
  // entities.push_back(&enemy);

  while (!WindowShouldClose()) {

    for (auto entity : entities) {
      entity->Move(GetFrameTime());
    }

    BeginDrawing();
    BeginMode2D(camera);
    ClearBackground(BLACK);

    map.Draw();
    player.Draw();
    camera.target = player.GetPosition();
    // enemy.Draw();

    EndMode2D();
    EndDrawing();
  }

  CloseWindow();

  return 0;
}
