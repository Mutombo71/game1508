#include "config.h"
#include "entity.h"
#include <fmt/core.h>
#include <raylib.h>
#include <raymath.h>

class Player : public Entity {
public:
  Player(Vector2 position)
      : position(position), velocity({Vector2Zero()}), maxSpeed(200.0f),
        acceleration(400.0f), deceleration(800.0f), currentFrame(0),
        frameTime(0.15f), elapsedTime(0.0f) {
    playerIdleTexture = LoadTexture("assets/Heroes/Rogue/Idle/Idle-Sheet.png");
    playerRunTexture = LoadTexture("assets/Heroes/Rogue/Run/Run-Sheet.png");
    frameIdleWidth = playerIdleTexture.width / 4;
    frameRunWidth = playerRunTexture.width / 12;
  }

  void Move(float deltaTime) {
    Vector2 targetVelocity = Vector2Zero();

    if (IsKeyDown(KEY_W))
      targetVelocity.y = -maxSpeed;
    if (IsKeyDown(KEY_S))
      targetVelocity.y = maxSpeed;
    if (IsKeyDown(KEY_A)) {
      targetVelocity.x = -maxSpeed;
      animationDir = -1;
    }
    if (IsKeyDown(KEY_D)) {
      targetVelocity.x = maxSpeed;
      animationDir = 1;
    }

    if (targetVelocity.x != 0.0f) {
      if (velocity.x < targetVelocity.x)
        velocity.x += acceleration * deltaTime;
      else if (velocity.x > targetVelocity.x)
        velocity.x -= deceleration * deltaTime;
    } else {
      if (velocity.x > 0.0f)
        velocity.x -= deceleration * deltaTime;
      else if (velocity.x < 0.0f)
        velocity.x += deceleration * deltaTime;
    }

    if (targetVelocity.y != 0.0f) {
      if (velocity.y < targetVelocity.y)
        velocity.y += acceleration * deltaTime;
      else if (velocity.y > targetVelocity.y)
        velocity.y -= deceleration * deltaTime;
    } else {
      if (velocity.y > 0.0f)
        velocity.y -= deceleration * deltaTime;
      else if (velocity.y < 0.0f)
        velocity.y += deceleration * deltaTime;
    }

    // Boundary checks
    if (this->position.x - 20.0f < 0) {
      this->position.x = 20.0f;
      this->velocity.x = 0.0f;
    } else if (this->position.x > screenWidth - 20.0f) {
      this->position.x = screenWidth - 20.0f;
      this->velocity.x = 0.0f;
    }

    if (this->position.y - 20.0f < 0) {
      this->position.y = 20.0f;
      this->velocity.y = 0.0f;
    } else if (this->position.y > screenHeight - 20.0f) {
      this->position.y = screenHeight - 20.0f;
      this->velocity.y = 0.0f;
    }

    this->position.x += velocity.x * deltaTime;
    this->position.y += velocity.y * deltaTime;

    // as do to c++ rounding "errors" the length of the velocity vector
    // never becomes exactly zero, so I define some epsilon, below which
    // I nullify the vector.
    if (Vector2Length(velocity) < 0.05) {
      velocity = Vector2Zero();
    }

    if (Vector2Length(velocity) != 0.0f) {
      calcCurrentFrame(deltaTime, 6, currentFrame);
    } else {
      calcCurrentFrame(deltaTime, 4, currentFrame);
    }
  }

  void Draw() {
    if (Vector2Length(velocity) != 0.0f) {
      // draw with move animation
      Rectangle source = {(float)(2 * currentFrame * frameRunWidth), 0,
                          animationDir * (float)frameRunWidth,
                          (float)playerRunTexture.height};
      DrawTexturePro(playerRunTexture, source,
                     Rectangle{position.x - (float)frameRunWidth / 2,
                               position.y - (float)playerRunTexture.height / 2,
                               (float)frameRunWidth,
                               (float)playerRunTexture.height},
                     Vector2{Vector2Zero()}, 0.0f, WHITE);
    } else {
      // draw with idle animation
      Rectangle source = {(float)(currentFrame * frameIdleWidth), 0,
                          animationDir * (float)frameIdleWidth,
                          (float)playerIdleTexture.height};
      DrawTexturePro(playerIdleTexture, source,
                     Rectangle{position.x - (float)frameIdleWidth / 2,
                               position.y - (float)playerIdleTexture.height / 2,
                               (float)frameIdleWidth,
                               (float)playerIdleTexture.height},
                     Vector2{Vector2Zero()}, 0.0f, WHITE);
    }
  }

  Vector2 GetPosition() { return position; }

private:
  Vector2 position;
  Vector2 velocity;
  float maxSpeed;
  float acceleration;
  float deceleration;
  // idle Texture
  Texture2D playerIdleTexture;
  // move Texture
  Texture2D playerRunTexture;

  int frameIdleWidth;
  int frameRunWidth;
  int currentFrame;
  float frameTime;
  float elapsedTime;
  int animationDir = 1;

  void calcCurrentFrame(float &deltaTime, int numberOfAnimationsInImage,
                        int &currentFrame) {
    elapsedTime += deltaTime;
    if (elapsedTime >= frameTime) {
      currentFrame = (currentFrame + 1) % numberOfAnimationsInImage;
      elapsedTime = 0.0f;
    }
  }
};
