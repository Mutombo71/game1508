#include "asset.h"
#include "config.h"
#include <experimental/random>
#include <fmt/core.h>
#include <fstream>
#include <nlohmann/json.hpp>
#include <raymath.h>

class Map {
public:
  Image img_tilemap;
  Map(){};

  void Draw() {

    int **tilemap = this->generateArray(mapHeight, mapWidth);

    for (int i = 0; i < mapWidth; i++) {
      for (int j = 0; j < mapHeight; j++) {
        switch (tilemap[j][i]) {
        case 0: {
          Vector2 position = {(float)i * tilesize, (float)j * tilesize};
          Rectangle source = {0, 0, tilesize, tilesize};
          DrawTextureRec(tileTexture, source, position, WHITE);
          break;
        }
        case 1: {
          Vector2 position = {(float)i * tilesize, (float)j * tilesize};
          Rectangle source = {tilesize, 0, tilesize, tilesize};
          DrawTextureRec(tileTexture, source, position, WHITE);
          break;
        }
        case 2: {
          Vector2 position = {(float)i * tilesize, (float)j * tilesize};
          Rectangle source = {2 * tilesize, 0, tilesize, tilesize};
          DrawTextureRec(tileTexture, source, position, WHITE);
          break;
        }
        case 3: {
          Vector2 position = {(float)i * tilesize, (float)j * tilesize};
          Rectangle source = {0, tilesize, tilesize, tilesize};
          DrawTextureRec(tileTexture, source, position, WHITE);
          break;
        }
        case 4: {
          Vector2 position = {(float)i * tilesize, (float)j * tilesize};
          Rectangle source = {tilesize, tilesize, tilesize, tilesize};
          DrawTextureRec(tileTexture, source, position, WHITE);
          break;
        }
        case 5: {
          Vector2 position = {(float)i * tilesize, (float)j * tilesize};
          Rectangle source = {2 * tilesize, tilesize, tilesize, tilesize};
          DrawTextureRec(tileTexture, source, position, WHITE);
          break;
        }
        case 6: {
          Vector2 position = {(float)i * tilesize, (float)j * tilesize};
          Rectangle source = {0, 2 * tilesize, tilesize, tilesize};
          DrawTextureRec(tileTexture, source, position, WHITE);
          break;
        }
        case 7: {
          Vector2 position = {(float)i * tilesize, (float)j * tilesize};
          Rectangle source = {tilesize, 2 * tilesize, tilesize, tilesize};
          DrawTextureRec(tileTexture, source, position, WHITE);
          break;
        }
        case 8: {
          Vector2 position = {(float)i * tilesize, (float)j * tilesize};
          Rectangle source = {2 * tilesize, 2 * tilesize, tilesize, tilesize};
          DrawTextureRec(tileTexture, source, position, WHITE);
          break;
        }
        }
      }
    }

    std::ifstream ifs("assets/Levels/levels1.json");
    auto levels = nlohmann::json::parse(ifs);

    fmt::println("{}", levels.dump());

    DrawTextureRec(propsTexture, assets["Chest2"], {600, 400}, WHITE);
  }

private:
  Texture2D treeTexture =
      LoadTexture("assets/Environment/Green Woods/Assets/Trees.png");
  Texture2D tileTexture =
      LoadTexture("assets/Environment/Green Woods/Assets/Tiles.png");
  Texture2D propsTexture =
      LoadTexture("assets/Environment/Green Woods/Assets/Props.png");

  int **generateArray(int rows, int columns) {
    // Allocate memory for the array of row pointers
    int **arr = new int *[rows];

    // Allocate memory for each row
    for (int i = 0; i < rows; ++i) {
      arr[i] = new int[columns];
    }

    // Initialize array elements
    int default_val = 4;
    for (int i = 0; i < rows; ++i) {
      for (int j = 0; j < columns; ++j) {
        if (i == 0) {
          arr[i][j] = 1;
        } else if (i == rows - 1) {
          arr[i][j] = 7;
        } else if (j == 0) {
          arr[i][j] = 3;
        } else if (j == columns - 1) {
          arr[i][j] = 5;
        } else {
          arr[i][j] = default_val;
        }
        arr[0][0] = 0;
        arr[0][columns - 1] = 2;
        arr[rows - 1][0] = 6;
        arr[rows - 1][columns - 1] = 8;
      }
    }

    return arr;
  }
};
